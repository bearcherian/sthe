package com.bearcherian.sonatype.utils;

public class StringUtils {
    /**
     * capitalize the first character in a String
     * @param text String to capitalize
     * @return String with the first letter capitalized.
     */
    public static String capitalize(String text) {
        Character a = text.charAt(0);
        return a.toString().toUpperCase() + text.substring(1);

    }
}
