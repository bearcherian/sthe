package com.bearcherian.sonatype.converter;

public class EnglishNumberToStringConverter {
    private static final String AND = "and";
    private static final String SPACE = " ";
    private static final String ZERO = "zero";
    private static final String HUNDRED = "hundred";
    private static final String[] NUMBERS = { ZERO, "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private static final String[] TENS = { "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private static final String[] THOU_MIL_TRIL = {"", "thousand", "million", "trillion"};
    private static final String NEGATIVE = "negative";

    public EnglishNumberToStringConverter(){

    }

    /**
     * Converts a 32-bit signed Integer to its English language cardinal string
     * @param number 32-bit Integer to convert
     * @return English cardinal number string value of @{number}. Value will be returned all lowercase
     */
    public String intToString(Integer number) {
        // zero is easy. no need to continue any further
        if (number.equals(0)) {
            return ZERO;
        }

        // the number will be processed from right to left, so we'll be using insert()
        // on the StringBuilder instead of append() in most cases
        StringBuilder numberInEnglish = new StringBuilder();

        // check if the number is negative, and then get the absolute value
        // using a Long because Math.abs(Integer.MIN_VALUE) > Integer.MAX_VALUE
        boolean isNegative = number < 0;
        Long currentPartialNumber = Math.abs(number.longValue());

        // process the number in groups of three (triad) so we can insert the proper word
        // for every x1000 group (thousand, million, billion, etc.)
        int triadNum = 0;
        String sep = "";
        while (triadNum == 0 || currentPartialNumber > 0) {
            int triad = ((Long)(currentPartialNumber % 1000)).intValue();

            // get the triad as a string. Identify if this is the first triad so we can add 'and' as needed
            String triadString = intTriadToString(triad, triadNum == 0);
            if (!"".equals(triadString)) {
                numberInEnglish
                        .insert(0, sep)
                        .insert(0, THOU_MIL_TRIL[triadNum])
                        .insert(0, sep)
                        .insert(0, triadString);
            }

            // remove the triad that was just processed
            currentPartialNumber = currentPartialNumber / 1000;
            sep = " ";
            triadNum++;
        }

        // prepend with the word 'negative' if the number was negative
        if (isNegative) {
            numberInEnglish.insert(0, NEGATIVE + " ");
        }

        // remove any trailing/leading spaces
        return numberInEnglish.toString().trim();
    }

    /**
     * Converts a 3 digit group to an String
     * @param number a 3 digit number (triad) to convert to a string.
     * @param firstTriad Flag to identify this triad as the first triad in a number. This is used to determine
     *                   if the word 'and' should be added
     * @return a string representation of the triad number
     */
    private String intTriadToString(Integer number, boolean firstTriad) {
        StringBuilder writtenNumber = new StringBuilder();

        // break the triad into each position to be processed individually
        int hundreds = number % 1000 / 100;
        int tens = number % 100 / 10;
        int ones = number % 10;

        // append the word 'hundred' to the English word for the hundreds digit
        if (hundreds > 0) {
            writtenNumber.append(NUMBERS[hundreds]).append(SPACE).append(HUNDRED).append(SPACE);

            // if we are in the first group, and the ones or tens position has a non-zero number,
            // then we add the word 'and' after 'hundred'
            if ( firstTriad && (tens > 0 || ones > 0)) {
                writtenNumber.append(AND).append(SPACE);
            }
        }

        // In the tens, we only process twenty and up, since 19 and below are single words that we handle
        // with the value in the ones position
        if (tens > 1) {
            writtenNumber.append(TENS[tens]).append(SPACE);
        }

        // if tens >= 10 or < 20, we add 10 to the ones so we can find the proper word from the ONES array
        if (tens == 1) {
            ones = ones + 10;
        }

        if (ones > 0){
            writtenNumber.append(NUMBERS[ones]).append(SPACE);
        }

        return writtenNumber.toString().trim();
    }
}
