package com.bearcherian.sonatype.converter.exec;

import com.bearcherian.sonatype.converter.EnglishNumberToStringConverter;
import com.bearcherian.sonatype.utils.StringUtils;

/**
 * Command line interface to enter and convert numeric integers to English values
 */
class NumberConverterExecutable {

    private static final String ERROR_MESSAGE_FORMAT = "\"%1$s\" is not a valid 32-bit Integer";

    public static void main(String[] args) {

        if (args.length <= 0) {
            System.out.println("To convert integers into strings, pass them as arguments to the application.\n" +
                    "Example: 'java -jar /path/to/jar 1234'");
        }

        EnglishNumberToStringConverter converter = new EnglishNumberToStringConverter();

        for (String arg : args) {
            String input = arg.replaceAll(",", "");
            try {
                System.out.println(StringUtils.capitalize(converter.intToString(Integer.parseInt(input))));
            } catch (NumberFormatException e) {
                System.out.println( String.format(ERROR_MESSAGE_FORMAT, arg));
            }
        }

    }

}
