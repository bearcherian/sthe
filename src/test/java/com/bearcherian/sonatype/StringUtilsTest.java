package com.bearcherian.sonatype;

import com.bearcherian.sonatype.utils.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringUtilsTest {

    @Test
    void SentenceCaseTest() {
        assertEquals("Lorem ipsum dolor",StringUtils.capitalize("lorem ipsum dolor"));
        assertEquals("A",StringUtils.capitalize("a"));
        assertEquals("12345",StringUtils.capitalize("12345"));
        assertEquals("ALL CAPS",StringUtils.capitalize("aLL CAPS"));
    }
}
