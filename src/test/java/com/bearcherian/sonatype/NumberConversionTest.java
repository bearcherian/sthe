package com.bearcherian.sonatype;

import com.bearcherian.sonatype.converter.EnglishNumberToStringConverter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberConversionTest {

    private final EnglishNumberToStringConverter converter = new EnglishNumberToStringConverter();

    @Test
    void ConversionTest() {

        assertEquals("thirteen", converter.intToString(13));
        assertEquals("eighty five", converter.intToString(85));
        assertEquals("five thousand two hundred and thirty seven", converter.intToString(5237));

        assertEquals("eighteen", converter.intToString(18));
        assertEquals("nineteen", converter.intToString(19));
        assertEquals("eleven", converter.intToString(11));
        assertEquals("ten", converter.intToString(10));
        assertEquals("thirteen thousand one hundred and forty seven", converter.intToString(13147));
    }

    @Test
    void ZeroesConversionTest() {
        assertEquals("zero",converter.intToString(0));
        assertEquals("one hundred", converter.intToString(100));
        assertEquals("fifty", converter.intToString(50));
        assertEquals("thirteen thousand", converter.intToString(13000));
        assertEquals("one million", converter.intToString(1000000));

    }

    @Test
    void MaxIntegerTest() {
        assertEquals( "two trillion one hundred forty seven million four hundred eighty three thousand six hundred and forty seven", converter.intToString(Integer.MAX_VALUE));
    }

    @Test
    void MinIntegerTest() {
        assertEquals( "negative two trillion one hundred forty seven million four hundred eighty three thousand six hundred and forty eight", converter.intToString(Integer.MIN_VALUE));
    }

    @Test
    void NegativeNumbersTest() {
        assertEquals("negative one", converter.intToString(-1));
        assertEquals("negative thirteen thousand one hundred and forty seven", converter.intToString(-13147));
        assertEquals("negative one hundred", converter.intToString(-100));
        assertEquals("negative fifty", converter.intToString(-50));
        assertEquals("negative thirteen thousand", converter.intToString(-13000));
        assertEquals("negative one million", converter.intToString(-1000000));
    }



}
