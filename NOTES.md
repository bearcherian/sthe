# Notes during development

## 2018-06-26

### Approach
My initial approach is going to be to take the number, modulo 10, which should give the number in the tens position. 
I can then divide the number by 10 and perform the action again.

For negative numbers, I'll need to use the absolute value.

I'll need to store the values in an array or enum

Since all 32-bit signed ints are valid, we need to support up to 2 trillion

### Questions:
* Need clarification on the use of the word 'and'? maybe?
* Output the word negative?


## 2018-06-27

Considering a new approach. Perhaps I can just convert to a string to be able to easily traverse back and forth within the number positions

I could go from left to right, but I'd have to check either the current position or look ahead to know if i'm in a hundred, or thousand, etc. But it'll be easier to look ahead and back. 
Going right to left is probably easier to tell what position i'm in mathematically speaking

internally, need to use a Long because the abs value of Integer.MIN_VALUE is greater than Integer.MAX_VALUE

Oh look we're getting somewhere
```
Expected :Negative Two Trillion One Hundred Forty Seven Million Six Hundred And Forty Eight
Actual   :Negative TwoOneFourSevenFourEightThreeSixFourEight
```

    looking for patterns
    index:  9 8 7 6 5 4 3 2 1 0
    number: 2 1 4 7 4 8 3 6 4 8

    Hundred: 2, 5, 8
    Thousand: 3
    Million: 6
    Trillion: 9
    
## 2018-06-28

I should split these into multiple groups for hundreds, thousands, millions and trillions, becaused

## 2018-06-30

The strings approach was not worth the effort. Going back to the math based method with modulus math.
