# Integer to English Converter
This application takes a 32-bit integer and converts it to an English string

| Input | Output |
| ----- | ------ |
| 0     | Zero   |
| 13    | Thirteen |
| 85    | Eighty five |
| 5237  | Five thousand two hundred and thirty seven |

## Building

Run `mvn clean package` to build a self executing jar that runs in the terminal. 

## Running

Run `java -jar /path/to/jar <numbers...>`. Where the numbers you want to convert to strings are passed as arguments